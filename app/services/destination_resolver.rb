# frozen_string_literal: true

class DestinationResolver
  class NoDestinationFound < StandardError; end

  attr_reader :product

  def perform(product:)
    @product = product

    return route_conditions.first.destination if route_conditions.any?

    raise NoDestinationFound, 'Could not find a suitable destination'
  end


  # Fetches any route that matches the product or category and does not exceed the
  # maximum price
  def route_conditions
    match = RouteCondition
      .where("? = ANY (product_ids)", product.id)
      .where("? = ANY (category_ids)", product.category_id)
      .where('max_price <= ?', product.price)

    return match if match.any?

    RouteCondition.where("? = ANY (product_ids)", product.id)
      .or(RouteCondition.where("? = ANY (category_ids)", product.category_id))
      .where('max_price <= ?', product.price)
  end
end
