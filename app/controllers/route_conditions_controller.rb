# frozen_string_literal: true

class RouteConditionsController < ApplicationController
  before_action :set_route_condition, only: %i[show edit update destroy]

  # GET /route_conditions
  # GET /route_conditions.json
  def index
    @route_conditions = RouteCondition.all
  end

  # GET /route_conditions/1
  # GET /route_conditions/1.json
  def show; end

  # GET /route_conditions/new
  def new
    @route_condition = RouteCondition.new
  end

  # GET /route_conditions/1/edit
  def edit; end

  # POST /route_conditions
  # POST /route_conditions.json
  def create
    @route_condition = RouteCondition.new(route_condition_params)

    respond_to do |format|
      if @route_condition.save
        format.html { redirect_to @route_condition, notice: 'Route condition was successfully created.' }
        format.json { render :show, status: :created, location: @route_condition }
      else
        format.html { render :new }
        format.json { render json: @route_condition.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /route_conditions/1
  # PATCH/PUT /route_conditions/1.json
  def update
    respond_to do |format|
      if @route_condition.update(route_condition_params)
        format.html { redirect_to @route_condition, notice: 'Route condition was successfully updated.' }
        format.json { render :show, status: :ok, location: @route_condition }
      else
        format.html { render :edit }
        format.json { render json: @route_condition.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /route_conditions/1
  # DELETE /route_conditions/1.json
  def destroy
    @route_condition.destroy
    respond_to do |format|
      format.html { redirect_to route_conditions_url, notice: 'Route condition was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_route_condition
    @route_condition = RouteCondition.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def route_condition_params
    params.require(:route_condition).permit!
  end
end
