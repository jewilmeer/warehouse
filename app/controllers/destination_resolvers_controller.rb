# frozen_string_literal: true

class DestinationResolversController < ApplicationController
  def new; end

  def create
    @product = Product.find_by(reference: params[:reference])
    @destination = DestinationResolver.new.perform(product: @product)

    # TODO: error handling for product not found
    # TODO: error handling when no destination could be found
  end
end
