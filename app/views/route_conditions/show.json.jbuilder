# frozen_string_literal: true

json.partial! 'route_conditions/route_condition', route_condition: @route_condition
