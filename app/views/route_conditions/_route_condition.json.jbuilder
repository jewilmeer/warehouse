# frozen_string_literal: true

json.extract! route_condition, :id, :created_at, :updated_at
json.url route_condition_url(route_condition, format: :json)
