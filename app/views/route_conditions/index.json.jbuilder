# frozen_string_literal: true

json.array! @route_conditions, partial: 'route_conditions/route_condition', as: :route_condition
