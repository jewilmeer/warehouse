# frozen_string_literal: true

class Product < ApplicationRecord
  validates :reference, presence: true, length: { maximum: 20 }
  validates :name, presence: true
  validates :price, presence: true
  validates :category, presence: true

  belongs_to :category

  after_destroy do
    # TODO: Make sure to remove the product from every route condition as well
  end
end
