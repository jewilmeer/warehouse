# frozen_string_literal: true

class RouteCondition < ApplicationRecord
  validates :destination, presence: true
  validates :max_price, presence: true
end
