# README

This application is able to determine the warehouse to which a product should be routed to.

## Setup

- Install ruby 2.7.x
- Setup postgres (on osx use Postgres.app for easy installation)
- run `script/setup`

## Running tests

`script/test` will run all tests

## Development

To run a development server run `script/server`, the server will be available on 
http://localhost:3000/
