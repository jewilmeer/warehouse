# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RouteConditionsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/route_conditions').to route_to('route_conditions#index')
    end

    it 'routes to #new' do
      expect(get: '/route_conditions/new').to route_to('route_conditions#new')
    end

    it 'routes to #show' do
      expect(get: '/route_conditions/1').to route_to('route_conditions#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/route_conditions/1/edit').to route_to('route_conditions#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/route_conditions').to route_to('route_conditions#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/route_conditions/1').to route_to('route_conditions#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/route_conditions/1').to route_to('route_conditions#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/route_conditions/1').to route_to('route_conditions#destroy', id: '1')
    end
  end
end
