# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'route_conditions/index', type: :view do
  let(:route_conditions) { create_list(:route_condition, 2)}

  before(:each) do
    assign(:route_conditions, route_conditions)
  end

  it 'renders a list of route_conditions' do
    render

    expect(rendered).to match(/#{route_conditions.first.destination}/)
    expect(rendered).to match(/#{route_conditions.last.destination}/)
  end
end
