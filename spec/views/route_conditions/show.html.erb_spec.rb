# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'route_conditions/show', type: :view do
  let(:route_condition) { create(:route_condition) }
  before(:each) do
    @route_condition = assign(:route_condition, route_condition)
  end

  it 'renders attributes in <p>' do
    render

    expect(rendered).to match(/#{route_condition.destination}/)
  end
end
