# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'products/show', type: :view do
  let(:product) { create(:product) }
  before(:each) do
    @product = assign(:product, product)
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(/#{product.reference}/)
    expect(rendered).to match(/#{product.name}/)
    expect(rendered).to match(/#{product.price}/)
  end
end
