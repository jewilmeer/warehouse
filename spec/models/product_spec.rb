# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Product, type: :model do
  context 'validations' do
    let(:product) { build :product }

    it 'is valid' do
      expect(product).to be_valid
    end

    describe 'reference' do
      it 'validates presence' do
        product.reference = nil
        expect(product).not_to be_valid
      end

      it 'does not allow more then 20 characters' do
        product.reference = SecureRandom.alphanumeric(21)
        expect(product).not_to be_valid
      end
    end

    describe 'name' do
      it 'validates presence' do
        product.name = nil
        expect(product).not_to be_valid
      end
    end

    describe 'price' do
      it 'validates presence' do
        product.price = nil
        expect(product).not_to be_valid
      end
    end

    describe 'category' do
      it 'validates presence' do
        product.category = nil
        expect(product).not_to be_valid
      end
    end
  end
end
