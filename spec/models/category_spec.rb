# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Category, type: :model do
  describe 'validations' do
    let(:category) { build :category }

    it 'is a valid record' do
      expect(category).to be_valid
    end

    it 'validates presence of name' do
      category.name = nil
      expect(category).not_to be_valid
    end
  end
end
