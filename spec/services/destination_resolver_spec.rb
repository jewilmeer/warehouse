# frozen_string_literal: true

require 'rails_helper'

describe DestinationResolver do
  subject(:destination) { resolver.perform(product: product) }

  let(:resolver) { described_class.new }
  let(:product) { create(:product) }

  before { create(:route_condition, :with_products) }

  context 'without a matching route conditions' do
    it 'raises an error' do
      expect { destination }.to \
        raise_error(DestinationResolver::NoDestinationFound)
    end
  end

  context 'with a reference match' do
    it 'returns the location of the route condition' do
      route_condition = create(:route_condition, product_ids: [product.id])
      expect(destination).to eql route_condition.destination
    end
  end

  context 'with a category match' do
    it 'returns the location of the route condition' do
      route_condition = create(:route_condition, category_ids: [product.category_id])

      expect(destination).to eql route_condition.destination
    end
  end

  context 'with multiple matching route conditions' do
    it 'matches the one with most matching criteria' do
      create(:route_condition, :with_products, :with_categories)
      create(:route_condition, product_ids: [product.id])
      route_condition = create(:route_condition, product_ids: [product.id], category_ids: [product.category_id])
      create(:route_condition, category_ids: [product.category])

      expect(destination).to eql route_condition.destination
    end
  end

  context 'max_price' do
    let!(:route_condition_part_match) do
      create(:route_condition, product_ids: [product.id])
    end

    let(:route_condition_full_match) do
      create(:route_condition,
             product_ids: [product.id],
             category_ids: [product.category_id])
    end

    it 'does not match if the price is too high' do
      route_condition_full_match.update(max_price: product.price + 1000)

      expect(destination).to eql route_condition_part_match.destination
    end

    it 'does match if the price is below max price' do
      route_condition_full_match.update(max_price: product.price)

      expect(destination).to eql route_condition_full_match.destination
    end
  end
end
