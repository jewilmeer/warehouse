# frozen_string_literal: true

FactoryBot.define do
  factory :product do
    reference { SecureRandom.alphanumeric }
    name { Faker::Commerce.product_name }
    price { rand(1_000_000) }

    category
  end
end
