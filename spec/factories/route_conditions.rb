# frozen_string_literal: true

FactoryBot.define do
  factory :route_condition do
    sequence(:destination) { |n| "Warehouse #{n}" }
    max_price { rand(10_000) }

    trait :with_categories do
      category_ids { create_list(:category, 2).map(&:id) }
    end

    trait :with_products do
      product_ids { create_list(:product, 2).map(&:id) }
    end
  end
end
