# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Allows easy use of create / create_list factory bot methods
include FactoryBot::Syntax::Methods # rubocop:disable Style/MixinUsage

# This part is only for development data
if Rails.env.development?
  create_list :route_condition, 2
  create_list :route_condition, 2, :with_products
  create_list :route_condition, 2, :with_categories
  create_list :route_condition, 2, :with_categories, :with_products
end
