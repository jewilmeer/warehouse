# frozen_string_literal: true

class CreateRouteConditions < ActiveRecord::Migration[6.1]
  def change
    create_table :route_conditions do |t|
      t.string :destination, null: false

      t.timestamps
    end
  end
end
