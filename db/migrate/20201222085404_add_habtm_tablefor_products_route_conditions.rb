# frozen_string_literal: true

class AddHabtmTableforProductsRouteConditions < ActiveRecord::Migration[6.1]
  def change
    create_table :products_route_conditions, id: false do |t|
      t.belongs_to :product
      t.belongs_to :route_condition
    end
  end
end
