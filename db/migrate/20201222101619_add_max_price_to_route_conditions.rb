# frozen_string_literal: true

class AddMaxPriceToRouteConditions < ActiveRecord::Migration[6.1]
  def change
    add_column :route_conditions, :max_price, :integer, null: false
  end
end
