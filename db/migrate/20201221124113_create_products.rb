# frozen_string_literal: true

class CreateProducts < ActiveRecord::Migration[6.1]
  def change
    create_table :products do |t|
      t.string :reference, null: false
      t.string :name, null: false
      t.integer :price, null: false
      t.references :category, null: false

      t.timestamps
    end
  end
end
