class RemoveHabtmTables < ActiveRecord::Migration[6.1]
  def change
    drop_table :categories_route_conditions
    drop_table :products_route_conditions
  end
end
