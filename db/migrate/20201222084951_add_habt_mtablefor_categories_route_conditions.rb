# frozen_string_literal: true

class AddHabtMtableforCategoriesRouteConditions < ActiveRecord::Migration[6.1]
  def change
    create_table :categories_route_conditions, id: false do |t|
      t.belongs_to :category
      t.belongs_to :route_condition
    end
  end
end
