# frozen_string_literal: true

class AlterTableRoutingConditions < ActiveRecord::Migration[6.1]
  def change
    change_table :route_conditions do |t|
      t.integer :category_ids, array: true, default: '{}'
      t.integer :product_ids, array: true, default: '{}'
    end

    add_index :route_conditions, :category_ids, using: :gin
    add_index :route_conditions, :product_ids, using: :gin
  end
end
