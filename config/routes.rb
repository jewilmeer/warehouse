# frozen_string_literal: true

Rails.application.routes.draw do
  resources :categories
  resources :products
  resources :route_conditions

  resource :destination_resolvers, only: %i[new create]

  root 'destination_resolvers#new'
end
